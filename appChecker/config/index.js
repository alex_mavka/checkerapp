/**
 * Created by fox on 06.02.15.
 */
module.exports = {
    db:{
        url: 'mongodb://127.0.0.1:27017/appchecker',
        db_conf:{
            hosts:[{"host": 'localhost', port:27017}],
            database:'appchecker'
        }
    }
};