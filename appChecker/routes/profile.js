/**
 * Created by fox on 07.02.15.
 */
var express = require('express');
var router = express.Router();
var BSON = require('mongodb').BSONPure;
router.get('/timefilter', function(req, res, next){
   if(req.cookies.id){
       var userId = new BSON.ObjectID(req.cookies.id);
       if(req.query.month){
           var now = (new Date()).getFullYear(),
               month = parseInt(req.query.month);

           var startDate = new Date(now, month-1, 1);
           var endDate = new Date(now, month, 1);
           req.db.collection('time').find({userId: userId, starttime:{$gte: startDate}, endtime:{$lt:endDate}}, function(err, dataTimes){
               dataTimes.toArray(function(err, items) {
                   res.render('profile-timefilter', {dates: items});
               });
           });
       } else {
           req.db.collection('time').find({userId: userId}, function(err, dataTimes){
               dataTimes.toArray(function(err, items) {
                   res.render('profile-timefilter', {dates: items});
               });
           });
       }
   }
});
router.get('/', function (req, res, next) {
    if (req.cookies.id) {
        console.log(req.cookies);
        var userId = new BSON.ObjectID(req.cookies.id);
        req.db.collection('users').findOne({'_id': userId}, function (err, data) {
            if (err) {
                console.log("Wrong id");
            }
            else {
                console.log(data);
                req.db.collection('time').find({userId: userId}, function(err, dataTimes){
                    dataTimes.toArray(function(err, items) {
                        res.render('profile', { email: data.email, dates: items});
                    });
                });

            }
        });
    }
    else {
        res.redirect('/');
    }
});

module.exports = router;