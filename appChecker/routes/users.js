var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var config = require('../config');
var BSON = require('mongodb').BSONPure;

/* GET users listing. */
router.post('/logout', function (req, res, next) {
    console.log('2e323r344t45t');
    res.clearCookie('id');
    res.status(200).end();
});
router.post('/action', function (req, res, next) {
    var userId = new BSON.ObjectID(req.cookies.id);
    req.db.collection('users').findOne({'_id': userId}, function (err, data) {
        if (err || !data) {
            console.log('bye');
        } else {
            var status = data.status;
            // status === true - user on work, false - out of work
            if (!status && req.body.action == "toWork") {
                var usertime = {userId: userId, starttime: new Date()};
                req.db.collection('time').insert(usertime, function (err, dataTime) {
                    if (err) {
                    } else {
                        debugger;
                        req.db.collection('users').update({'_id': userId}, {$set: {status: true, lastLogRecordId: dataTime[0]._id.toString()}}, function (err, data) {
                            if (err) {
                            } else {
                                res.send({"status": "ok", "data": data, dataTime: dataTime});
                            }
                        })
                    }
                });


            } else if (status && req.body.action == "fromWork") {
                req.db.collection('users').findOne({_id: userId}, function(err, userData){
                    var timeId = new BSON.ObjectID(userData.lastLogRecordId);
                    req.db.collection('time').update({_id:timeId}, {$set:{endtime: new Date()}}, function(err, dataTime){
                        if (err){

                        } else {
                            req.db.collection('users').update({_id:userId}, {$set:{status:false}}, function(err, dataUser){
                                if (err){

                                } else {
                                    res.send({"status":"ok", data:dataTime});
                                }
                            })
                        }
                    })
                });
            }
            else {
                res.send({"status": "error", "message": "you're at work"});
            }
        }
    })

    if (req.body.action == "toWork") {

    } else if (req.body.action == "fromWork") {

    }
    else {
        res.send(502);
    }
});
router.post('/register', function (req, res, next) {
    var regularExp = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
    var passRegEx = /^[a-zA-Z0-9\.\+\=\-\_!?]{4,20}$/
    if (req.body.passconfirm !== req.body.pass) {
        res.send({"status": "error", "message": "passwords mismatch"});
    } else
    //  MongoClient.connect(config.db.url, function (err, db) {
    if (!passRegEx.test(req.body.pass)) {
        res.send({"status": "error", "message": "sorry, but pswrd is invalid"})
    } else if (!regularExp.test(req.body.email)) {
        res.send({"status": "error", "message": "Sorry, but... "});
    } else {
        console.log("wow! We're connected!");
        //var hash = md5(req.body.pass)
        req.db.collection('users').findOne({"email": req.body.email}, function (err, data) {
            if (err) {
                console.log("error");
            }
            else if (data) {
                console.log("Hey! No tricks");
                res.send(502);
            }
            else {
                var user = {"email": req.body.email, "pass": req.body.pass}
                req.db.collection('users').insert(user, function (err, data) {
                    if (err) {
                        console.log('Database wasn\'t added', err);
                    }
                    else {
                        console.log("Wooow!! You're amazing!");
                        console.log("qwe", data);
                        res.cookie('id', data[0]._id.toString(), {httpOnly: true});
                        res.status(200).end();
                    }
                });
            }

        });
    }
    //   });
});
router.post('/login', function (req, res, next) {
    var regularExp = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
    var passRegEx = /^[a-zA-Z0-9\.\+\=\-\_!?]{4,20}$/
    console.log(123)
    if (!passRegEx.test(req.body.pass)) {
        console.log(1);
        res.send({"status": "error", "message": "sorry, but pswrd is invalid"})
    } else if (!regularExp.test(req.body.email)) {
        console.log(2);
        res.send({"status": "error", "message": "Sorry, but... "});
    } else {
        console.log("wow! We're connected!");
        req.db.collection('users').findOne({"email": req.body.email}, function (err, data) {
            if (err) {
                console.log("error");
            }
            else if (data) {
                console.log("Hey! No tricks");
                if (data.pass === req.body.pass) {
                    res.cookie('id', data._id.toString(), {httpOnly: true});
                    res.send({status: "ok", next: "/profile"});
                } else {
                    res.send({"status": "error", "message": "passwords mismatch"});
                }
            }

            else {
                res.send({"status": "error", "message": "user not found"})
            }
        });
    }
});

module.exports = router;
