/**
 * Created by fox on 06.02.15.
 */
window.app = {
    "signUp": function () {
        var email = $('#inputEmail').val();
        var pass = $('#inputPassword').val();
        var passconfirm = $('#confirmPassword').val();
        console.log(3, pass, passconfirm);
        if (pass == passconfirm) {
            $.ajax({
                type: "POST",
                url: '/users/register',
                data: {"email": email, "pass": pass, passconfirm: passconfirm},
                success: function () {
                    console.log(1);
                    console.log(arguments);
                    window.location = '/profile';
                },
                fail: function () {
                    console.log(2);
                    console.log(arguments);
                }
            });
        }
        else {
            console.log(4);
            $('#errorModal').modal('show')
        }
        return false; // prevent default action

    },
    "signIn": function () {
        var email = $('#inputEmailSI').val();
        var pass = $('#inputPasswordSI').val();

        $.ajax({
            type: "POST",
            url: '/users/login',
            data: {"email": email, "pass": pass},
            success: function (result, status) {
                if (!result || result && result.status === "error") {
                    alert(result.message);
                } else {
                    if (result && result.status === "ok") {
                        if (result.next) {
                            window.location = result.next;
                        }
                    }
                }
                console.log(arguments);
            },
            fail: function () {
                console.log(arguments);
            }
        });

        return false; // prevent default action

    },
    "logout": function () {
        $.ajax({
            type: "POST",
            url: '/users/logout',
            success: function () {
                window.location = '/';
            },
            fail: function () {
                console.log(arguments);
            }
        })
    },
    "toWork": function () {
        $.ajax({
            type: "POST",
            url: '/users/action',
            data: {"action": "toWork"},
            success: function (result) {
                if (result.status !== "error") {
                    alert('hello, username');
                } else {
                    alert("Nohello, dude!")
                }
            },
            fail: function () {
                console.log(arguments);
            }
        })
    },
    "fromWork": function () {
        $.ajax({
            type: "POST",
            url: '/users/action',
            data: {"action": "fromWork"},
            success: function (result) {
                if (result.status !== "error") {
                    alert('go out, username');
                } else {
                    alert("bye, dude!")
                }
            },
            fail: function () {
                console.log(arguments);
            }
        })
    },
    "timeFilter": function (monthnum) {
        $.ajax({
            type: "GET",
            url: '/profile/timefilter',
            data: {"month": monthnum},
            success: function (result) {
                $("#datacontent").html(result);
            },
            fail: function(){
                console.log("oh! Your data is INVALID!");
            }
        })
    }
};